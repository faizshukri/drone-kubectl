# Drone Kubectl

```sh
kubectl apply -f rbac.yaml

# cert
kubectl get secret drone-deploy-token-xxx -o jsonpath='{.data.ca\.crt}' && echo

# token
kubectl get secret drone-deploy-token-xxx -o jsonpath='{.data.token}' | base64 --decode && echo
```
