FROM alpine:3.10

RUN apk --no-cache add curl ca-certificates bash

RUN mkdir -p /opt/kubernetes/bin && \
    curl -Lo /opt/kubernetes/bin/kubectl \
    https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && \
    chmod +x /opt/kubernetes/bin/kubectl

COPY init-kubectl kubectl /opt/faizshukri/kubectl/bin/

ENV PATH="/opt/faizshukri/kubectl/bin/:$PATH"

ENTRYPOINT ["kubectl"]

CMD ["--help"]
